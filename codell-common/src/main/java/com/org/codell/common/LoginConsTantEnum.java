package com.org.codell.common;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

public interface LoginConsTantEnum {
    /**
     * 登录类型
     */
    @Getter
    @AllArgsConstructor
    enum TypeEnum{
        QQ_ENUM(1,"qq","loginQQServiceImpl"),
        GITHUB_ENUM(2,"github","loginGithubServiceImpl");


        private Integer value;
        private String name;
        private String className;

        //根据传入的name 返回指定的service
        public static TypeEnum getInstance(String type){
            if(type==null){
                return null;
            }
            for (TypeEnum typeEnum : TypeEnum.values()){
                if(Objects.deepEquals(typeEnum.getName(), type)){
                    return typeEnum;
                }
            }
            return null;
        }


    }
}
