package com.org.codell.dmg.mapper;

import com.org.codell.dmg.model.UmsOauthGithub;
import com.org.codell.dmg.model.UmsOauthGithubExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsOauthGithubMapper {
    long countByExample(UmsOauthGithubExample example);

    int deleteByExample(UmsOauthGithubExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UmsOauthGithub record);

    int insertSelective(UmsOauthGithub record);

    List<UmsOauthGithub> selectByExample(UmsOauthGithubExample example);

    UmsOauthGithub selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UmsOauthGithub record, @Param("example") UmsOauthGithubExample example);

    int updateByExample(@Param("record") UmsOauthGithub record, @Param("example") UmsOauthGithubExample example);

    int updateByPrimaryKeySelective(UmsOauthGithub record);

    int updateByPrimaryKey(UmsOauthGithub record);
}