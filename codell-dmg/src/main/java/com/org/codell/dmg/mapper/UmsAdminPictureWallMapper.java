package com.org.codell.dmg.mapper;

import com.org.codell.dmg.model.UmsAdminPictureWall;
import com.org.codell.dmg.model.UmsAdminPictureWallExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsAdminPictureWallMapper {
    long countByExample(UmsAdminPictureWallExample example);

    int deleteByExample(UmsAdminPictureWallExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UmsAdminPictureWall record);

    int insertSelective(UmsAdminPictureWall record);

    List<UmsAdminPictureWall> selectByExample(UmsAdminPictureWallExample example);

    UmsAdminPictureWall selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UmsAdminPictureWall record, @Param("example") UmsAdminPictureWallExample example);

    int updateByExample(@Param("record") UmsAdminPictureWall record, @Param("example") UmsAdminPictureWallExample example);

    int updateByPrimaryKeySelective(UmsAdminPictureWall record);

    int updateByPrimaryKey(UmsAdminPictureWall record);
}