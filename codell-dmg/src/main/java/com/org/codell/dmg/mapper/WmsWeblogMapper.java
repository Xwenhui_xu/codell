package com.org.codell.dmg.mapper;

import com.org.codell.dmg.model.WmsWeblog;
import com.org.codell.dmg.model.WmsWeblogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WmsWeblogMapper {
    long countByExample(WmsWeblogExample example);

    int deleteByExample(WmsWeblogExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WmsWeblog record);

    int insertSelective(WmsWeblog record);

    List<WmsWeblog> selectByExample(WmsWeblogExample example);

    WmsWeblog selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WmsWeblog record, @Param("example") WmsWeblogExample example);

    int updateByExample(@Param("record") WmsWeblog record, @Param("example") WmsWeblogExample example);

    int updateByPrimaryKeySelective(WmsWeblog record);

    int updateByPrimaryKey(WmsWeblog record);
}