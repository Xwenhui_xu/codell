package com.org.codell.dmg.mapper;

import com.org.codell.dmg.model.UmsOauthQq;
import com.org.codell.dmg.model.UmsOauthQqExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsOauthQqMapper {
    long countByExample(UmsOauthQqExample example);

    int deleteByExample(UmsOauthQqExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UmsOauthQq record);

    int insertSelective(UmsOauthQq record);

    List<UmsOauthQq> selectByExample(UmsOauthQqExample example);

    UmsOauthQq selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UmsOauthQq record, @Param("example") UmsOauthQqExample example);

    int updateByExample(@Param("record") UmsOauthQq record, @Param("example") UmsOauthQqExample example);

    int updateByPrimaryKeySelective(UmsOauthQq record);

    int updateByPrimaryKey(UmsOauthQq record);
}