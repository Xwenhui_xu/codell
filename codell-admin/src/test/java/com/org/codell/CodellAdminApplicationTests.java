package com.org.codell;


import com.org.codell.service.ums.UmsRoleService;
import com.org.codell.utils.URLEncodeUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)  //public final class SpringRunner extends SpringJUnit4ClassRunner
@SpringBootTest
public class CodellAdminApplicationTests {

    @Resource
    UmsRoleService umsRoleService;

    @Value("${prop.upload-folder}")
    private String url;
    @Test
   public void contextLoads() {
        List<Long> menuIds = new ArrayList<>();
        menuIds.add(1L);
        umsRoleService.allocMenu(3L,menuIds);

    }

    @Test
    public void Content(){

        System.out.println(URLEncodeUtil.getURLEncoderString("http://127.0.0.1:8080/QQLogin"));

        System.out.println(URLEncodeUtil.getURLEncoderString("http://127.0.0.1:8095/oauth/qq/callback"));

        System.out.println(URLEncodeUtil.getURLEncoderString("http://www.codell.cloud:8095/oauth/qq/callback"));

    }

}
