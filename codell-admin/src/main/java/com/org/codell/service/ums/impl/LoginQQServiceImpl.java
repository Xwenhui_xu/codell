package com.org.codell.service.ums.impl;

import com.org.codell.common.tools.CommonResult;
import com.org.codell.config.Constants;
import com.org.codell.dao.ums.UmsOauthDao;
import com.org.codell.dmg.mapper.UmsAdminMapper;
import com.org.codell.dmg.mapper.UmsOauthQqMapper;
import com.org.codell.dmg.model.UmsAdmin;
import com.org.codell.dmg.model.UmsAdminExample;
import com.org.codell.dmg.model.UmsOauthQq;
import com.org.codell.dmg.model.UmsOauthQqExample;
import com.org.codell.dto.AccessTokenDTO;
import com.org.codell.dto.QQUser;
import com.org.codell.dto.UmsAdminParam;
import com.org.codell.provider.QQProvider;
import com.org.codell.security.utils.JwtTokenUtil;
import com.org.codell.service.ums.OuathLoginService;
import com.org.codell.service.ums.UmsAdminService;
import com.org.codell.utils.HttpClientUtils;
import com.org.codell.utils.URLEncodeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class LoginQQServiceImpl implements OuathLoginService {
    @Autowired
    UmsAdminService adminService;
    @Autowired
    UmsAdminMapper adminMapper;
    @Autowired
    UmsOauthQqMapper oauthQqMapper;
    @Autowired
    UmsOauthDao oauthDao;
    @Autowired
    QQProvider qqProvider;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private Constants constants;

    @Override
    public Map<String, Object> checkUser(String code, String state) throws Exception {
        log.info("qq用户校验完成");
        //获取tocket
        Map<String, Object> qqProperties = qqProvider.getToken(code);
        //获取openId(每个用户的openId都是唯一不变的)
        String openId = qqProvider.getOpenId(qqProperties);
        qqProperties.put("openId", openId);
        //tocket过期刷新token
        //Map<String, Object> refreshToken = refreshToken(qqProperties);

        //返回结果
        Map<String, Object> res = new HashMap<>();
        //获取数据
        QQUser userInfo = qqProvider.getUserInfo(qqProperties);
        //判断 当前账号登录是否已关联站内账号
        UmsOauthQqExample example = new UmsOauthQqExample();
        example.createCriteria().andOpenIdEqualTo(openId);
        List<UmsOauthQq> user = oauthQqMapper.selectByExample(example);
        //QQ新用户 关联系统
        if (user.size() <= 0) {
            log.info("存储QQ账号信息");
            UmsOauthQq oauthQq = new UmsOauthQq();
            oauthQq.setCreateTime(new Date());
            oauthQq.setYear(userInfo.getYear());
            oauthQq.setGender(userInfo.getGender());
            oauthQq.setImgUrl(userInfo.getFigureurl_qq());
            oauthQq.setOpenId(openId);
            oauthQq.setNickName(userInfo.getNickname());
            oauthQqMapper.insert(oauthQq);
            res.put("msg", "newAccount");
            res.put("token", oauthQq.getId());
            return res;
        }
        // QQ账号未和站内站内账号关联
        if (user.get(0).getBelongId() == null) {
            res.put("msg", "newAccount");
            res.put("token", user.get(0).getId());
            return res;
        }
        //正常登录返回 JWT令牌
        String token = adminService.oauthLogin(user.get(0).getBelongId());
        res.put("msg", "ok");
        res.put("token", token);
        return res;
    }

    @Override
    public String getCodeByType() throws Exception {
        //拼接返回的URL
        StringBuilder url = new StringBuilder();
        url.append("https://graph.qq.com/oauth2.0/authorize?");
        url.append("response_type=code");
        url.append("&client_id=" + constants.getQqClientId());
        //回调地址 ,回调地址要进行Encode转码
        String redirect_uri = constants.getQqRedirectUri();
        //转码
        url.append("&redirect_uri=" + URLEncodeUtil.getURLEncoderString(redirect_uri));
        url.append("&state=ok");
        //请求QQ第三方地址
        HttpClientUtils.get(url.toString(), "UTF-8");
        return url.toString();
    }


}
