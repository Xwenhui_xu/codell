package com.org.codell.service.ums;

import com.org.codell.dmg.model.UmsAdminPictureWall;
import com.org.codell.dto.UmsAdminPictureWallParam;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 照片墙 Service
 */
public interface UmsAdminPictureWallService {

    /**
     * 批量 新增照片墙
     * @param record
     * @return
     */
    @Transactional
    Integer addPictureWallList(UmsAdminPictureWallParam record);

    /**
     * 根据 adminId 查询照片墙
     * @param adminId
     * @return
     */
    List<UmsAdminPictureWall> list(Long adminId);
}
