package com.org.codell.service.sms;

import com.org.codell.dmg.model.WmsWeblog;

import java.util.List;

public interface SmsWebLogApiService {
    /**
     * 获取请求追踪日志列表
     * @param keyword
     * @param pageSize
     * @param pageNum
     * @return
     */
    List<WmsWeblog> list(String keyword, Integer pageNum, Integer pageSize);

    /**
     * 删除请求追踪日志列表
     *
     * @param ids 日志 id集合
     */
    int delete(List<Integer> ids);

}
