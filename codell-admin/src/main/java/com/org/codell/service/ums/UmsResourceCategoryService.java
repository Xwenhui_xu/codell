package com.org.codell.service.ums;

import java.util.List;
import com.org.codell.dmg.model.UmsResourceCategory;
public interface UmsResourceCategoryService {
    /**
     * 查询所有资源分类
     * @return
     */
    List<UmsResourceCategory> listAll();

    /**
     * 创建资源分类
     */
    int create(UmsResourceCategory umsResourceCategory);

    /**
     * 修改资源分类
     */
    int update(Long id, UmsResourceCategory umsResourceCategory);

    /**
     * 删除资源分类
     */
    int delete(Long id);
}
