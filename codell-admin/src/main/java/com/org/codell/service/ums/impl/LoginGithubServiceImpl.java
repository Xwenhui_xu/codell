package com.org.codell.service.ums.impl;

import com.org.codell.config.Constants;
import com.org.codell.dto.AccessTokenDTO;
import com.org.codell.dto.GithubUser;
import com.org.codell.provider.GithubProvider;
import com.org.codell.service.ums.OuathLoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class LoginGithubServiceImpl  implements OuathLoginService {
    @Autowired
    private GithubProvider githubProvider;
    @Autowired
    private Constants constants;


    @Override
    public Map<String,Object> checkUser(String code, String state) throws Exception {
        log.info("github用户校验完成");
        AccessTokenDTO accessTokenDto = new AccessTokenDTO();
        accessTokenDto.setCode(code);
        accessTokenDto.setState(state);
        accessTokenDto.setRedirect_uri(constants.getGithubRedirectUri());
        accessTokenDto.setClient_id(constants.getGithubClientId());
        accessTokenDto.setClient_secret(constants.getGithubClientSecret());
        String accessToken = githubProvider.getAccessToken(accessTokenDto);
        System.out.println(accessTokenDto);
        GithubUser githubUser = githubProvider.getUser(accessToken);
        if(githubUser != null && githubUser.getId() != null){
            log.info(String.valueOf(githubUser));
        }else{
        }
        Map<String,Object> res = new HashMap<>();
        res.put("userLogin",githubUser);
        return res;


    }

    @Override
    public String getCodeByType() {
        return null;
    }
}
