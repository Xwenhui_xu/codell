package com.org.codell.service.ums.impl;

import com.github.pagehelper.PageHelper;
import com.org.codell.dao.ums.UmsLoginLogDao;
import com.org.codell.dmg.mapper.UmsAdminLoginLogMapper;
import com.org.codell.dmg.model.UmsAdmin;
import com.org.codell.dmg.model.UmsAdminLoginLogExample;
import com.org.codell.service.ums.UmsLoginLogService;
import com.org.codell.vo.UmsLoginLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class UmsLoginLogServiceImpl implements UmsLoginLogService {
    @Autowired
    private UmsLoginLogDao umsLoginLogDao;
    @Autowired
    private UmsAdminLoginLogMapper loginLogMapper;

    /**
     * 查询登录日志
     * @param keyword        用户名
     * @param loginTimeFrom  开始时间
     * @param loginTimeTo    结束时间
     * @param pageNum
     * @param pageSize
     * @return
     */

    @Override
    public List<UmsLoginLogVo> list(String keyword, String loginTimeFrom, String loginTimeTo, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        // 传入值:开始时间:2020-09-15  结束时间：""; 默认 loginTimeTo :系统当天日期
        if((loginTimeFrom!=null && loginTimeFrom!="") && (loginTimeTo==null || loginTimeTo=="")){
            //设置结束日期
            loginTimeTo=dateFormat.format(date);
            //判断开始日期是否等于 当天系统日期
            if(loginTimeFrom.equals(loginTimeTo)){
                loginTimeTo=dateFormat.format(date)+" 23:59:59";
            }
        }
        // 传入值:开始时间:"" 结束时间:2020-09-15; 默认:loginTimeFrom:获取当前日期的月份 第一天，如2020-09-01;loginTimeTo最大值不得超过当天日期
        if((loginTimeFrom==null || loginTimeFrom=="") && (loginTimeTo!=null && loginTimeTo!="")){
            // 开始日期为: "",设置开始日期,默认当月1号
            SimpleDateFormat dateFormatFrom = new SimpleDateFormat("yyyy-MM");
            loginTimeFrom=dateFormatFrom.format(date)+"-01";
            //判断结束日期是否 等于当天日期
            if(loginTimeTo.equals(loginTimeTo)){
                loginTimeTo=dateFormat.format(date)+" 23:59:59";
            }
        }
        //当都传入值时
        if( (loginTimeFrom!=null && loginTimeFrom!="") && (loginTimeTo!=null && loginTimeTo!="") ){
            loginTimeFrom+=" 00:00:00";
            loginTimeTo+=" 23:59:59";
        }
        List<UmsLoginLogVo> umsLoginLogVoList = umsLoginLogDao.getList(keyword,loginTimeFrom,loginTimeTo);
        return umsLoginLogVoList;
    }

    @Override
    public int delete(List<Long> ids) {
        UmsAdminLoginLogExample example = new UmsAdminLoginLogExample();
        UmsAdminLoginLogExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(ids);
        int count = loginLogMapper.deleteByExample(example);
        return count;
    }

    @Override
    public Long findTotalVisitCount() {
        Long count = loginLogMapper.countByExample(new UmsAdminLoginLogExample());
        return count;
    }

    @Override
    public Long findTodayVisitCount() {
        return umsLoginLogDao.findTodayVisitCount();
    }

    @Override
    public Long findTodayIp() {
        return umsLoginLogDao.findTodayIp();
    }

    @Override
    public List<Map<String, Object>> findLastSevenDaysVisitCount(String username) {
        return umsLoginLogDao.findLastSevenDaysVisitCount(username);
    }

    /**
     * 获取系统近10天来的访问记录
     *
     * @return 系统近七天来的访问记录
     */
    @Override
    public List<Map<String, Object>> findLastTenDaysVisitCount(String username){
        return umsLoginLogDao.findLastTenDaysVisitCount(username);
    }

    @Override
    public List<Map<String, Object>> findTheMonthByWeeksTotal(String username) {
        return umsLoginLogDao.findTheMonthByWeeksTotal(username);
    }
}
