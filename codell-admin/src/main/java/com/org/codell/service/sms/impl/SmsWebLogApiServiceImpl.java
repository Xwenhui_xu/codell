package com.org.codell.service.sms.impl;

import com.github.pagehelper.PageHelper;
import com.org.codell.dmg.mapper.WmsWeblogMapper;
import com.org.codell.dmg.model.WmsWeblog;
import com.org.codell.dmg.model.WmsWeblogExample;
import com.org.codell.service.sms.SmsWebLogApiService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SmsWebLogApiServiceImpl implements SmsWebLogApiService {
    @Resource
    private WmsWeblogMapper weblogMapper;

    @Override
    public List<WmsWeblog> list(String keyword, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        WmsWeblogExample example = new WmsWeblogExample();
        WmsWeblogExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause("startTime desc");
        if(!StringUtils.isEmpty(keyword)){
            criteria.andUsernameEqualTo(keyword);
        }
        List<WmsWeblog> wmsWeblogs = weblogMapper.selectByExample(example);
        return wmsWeblogs;
    }

    @Override
    public int delete(List<Integer> ids) {
        WmsWeblogExample example = new WmsWeblogExample();
        WmsWeblogExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(ids);
        int count = weblogMapper.deleteByExample(example);
        return count;
    }
}
