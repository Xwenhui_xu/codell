package com.org.codell.service.ums;

import com.org.codell.dmg.model.UmsAdmin;
import com.org.codell.vo.UmsLoginLogVo;

import java.util.List;
import java.util.Map;

public interface UmsLoginLogService {

    /**
     * 获取登录日志列表
     * @param keyword
     * @param loginTimeFrom
     * @param loginTimeTo
     * @param pageSize
     * @param pageNum
     * @return
     */
    List<UmsLoginLogVo> list(String keyword, String loginTimeFrom, String loginTimeTo, Integer pageNum, Integer pageSize);

    /**
     * 删除登录日志
     *
     * @param ids 日志 id集合
     */
    int delete(List<Long> ids);
    /**
     * 获取系统总访问次数
     *
     * @return Long
     */
    Long findTotalVisitCount();

    /**
     * 获取系统今日访问次数
     *
     * @return Long
     */
    Long findTodayVisitCount();

    /**
     * 获取系统今日访问 IP数
     *
     * @return Long
     */
    Long findTodayIp();

    /**
     * 获取系统近七天来的访问记录
     *
     * @return 系统近七天来的访问记录
     */
    List<Map<String, Object>> findLastSevenDaysVisitCount(String username);

    /**
     * 获取系统近15天来的访问记录
     *
     * @return 系统近七天来的访问记录
     */
    List<Map<String, Object>> findLastTenDaysVisitCount(String username);

    /**
     * 按本月统计 周一至周五的所有请求数据
     * @param username
     * @return
     */
    List<Map<String,Object>> findTheMonthByWeeksTotal(String username);
}
