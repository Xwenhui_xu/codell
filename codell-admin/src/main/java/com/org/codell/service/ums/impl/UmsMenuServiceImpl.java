package com.org.codell.service.ums.impl;

import com.github.pagehelper.PageHelper;
import com.org.codell.dmg.mapper.UmsMenuMapper;
import com.org.codell.dmg.model.UmsMenu;
import com.org.codell.dmg.model.UmsMenuExample;
import com.org.codell.dto.UmsMenuNode;
import com.org.codell.service.ums.UmsMenuService;
import com.org.codell.service.ums.UmsRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UmsMenuServiceImpl implements UmsMenuService {

    @Autowired
    private UmsMenuMapper menuMapper;


    @Override
    public List<UmsMenu> list(Long parentId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        UmsMenuExample umsMenuExample = new UmsMenuExample();
        umsMenuExample.setOrderByClause("sort desc");
        umsMenuExample.createCriteria().andParentIdEqualTo(parentId);
        return menuMapper.selectByExample(umsMenuExample);
    }

    @Override
    public int create(UmsMenu umsMenu) {
        umsMenu.setCreateTime(new Date());
        updateLevel(umsMenu);
        return menuMapper.insert(umsMenu);
    }
    /**
     * 修改菜单层级
     */
    private void updateLevel(UmsMenu umsMenu) {
        if (umsMenu.getParentId() == 0) {
            //没有父菜单时为一级菜单
            umsMenu.setLevel(0);
        } else {
            //有父菜单时选择根据父菜单level设置
            UmsMenu parentMenu = menuMapper.selectByPrimaryKey(umsMenu.getParentId());
            if (parentMenu != null) {
                umsMenu.setLevel(parentMenu.getLevel() + 1);
            } else {
                umsMenu.setLevel(0);
            }
        }
    }
    @Override
    public int update(Long id, UmsMenu umsMenu) {
        umsMenu.setId(id);
        updateLevel(umsMenu);
        return menuMapper.updateByPrimaryKeySelective(umsMenu);
    }
    @Override
    public UmsMenu getItem(Long id) {
        return menuMapper.selectByPrimaryKey(id);
    }

    @Override
    public int delete(Long id) {
        return menuMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateHidden(Long id, Integer hidden) {
        UmsMenu umsMenu = new UmsMenu();
        umsMenu.setId(id);
        umsMenu.setHidden(hidden);
        return menuMapper.updateByPrimaryKeySelective(umsMenu);
    }


    @Override
    public List<UmsMenuNode> treeList(){
        List<UmsMenu> menuList = menuMapper.selectByExample(new UmsMenuExample());
        List<UmsMenuNode> result = menuList.stream()
                .filter(umsMenu -> umsMenu.getParentId().equals(0L))
                .map(umsMenu -> covertMenuTreeNode(umsMenu,menuList))
                .collect(Collectors.toList());
        return result;
    }

    private UmsMenuNode covertMenuTreeNode(UmsMenu umsMenu,List<UmsMenu> menuList){
        UmsMenuNode umsMenuNode = new UmsMenuNode();
        BeanUtils.copyProperties(umsMenu,umsMenuNode);
        List<UmsMenuNode> children = menuList.stream()
                .filter(subMenu -> subMenu.getParentId().equals(umsMenu.getId()))
                .map(subMenu -> covertMenuTreeNode(subMenu,menuList))
                .collect(Collectors.toList());
        umsMenuNode.setChildren(children);
        return umsMenuNode;
    }
}
