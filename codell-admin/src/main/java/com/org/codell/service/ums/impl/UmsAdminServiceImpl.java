package com.org.codell.service.ums.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.org.codell.Aspect.AdminUserDetails;
import com.org.codell.dao.ums.UmsRelationAdminRoleDao;
import com.org.codell.dmg.mapper.UmsAdminLoginLogMapper;
import com.org.codell.dmg.mapper.UmsAdminMapper;
import com.org.codell.dmg.mapper.UmsOauthQqMapper;
import com.org.codell.dmg.mapper.UmsRelationAdminRoleMapper;
import com.org.codell.dmg.model.*;
import com.org.codell.dto.UmsAdminParam;
import com.org.codell.dto.UmsLoginLogParam;
import com.org.codell.dto.UpdateAdminPasswordParam;
import com.org.codell.security.utils.JwtTokenUtil;
import com.org.codell.service.ums.UmsAdminService;
import com.org.codell.utils.HttpContextUtil;
import com.org.codell.utils.IpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * UmsAdminService实现类
 */
@Service
public class UmsAdminServiceImpl implements UmsAdminService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UmsAdminServiceImpl.class);
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Resource
    private UmsAdminMapper adminMapper;
    @Autowired
    private UmsRelationAdminRoleDao adminRoleRelationDao;
    @Autowired
    private UmsRelationAdminRoleMapper roleMapper;
    @Autowired
    private UmsAdminLoginLogMapper loginLogMapper;
    @Autowired
    private UmsOauthQqMapper oauthQqMapper;
    @Autowired
    private UmsRelationAdminRoleMapper relationAdminRoleMapper;

    @Override
    public UmsAdmin getAdminByUsername(String username) {
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(username);
        List<UmsAdmin> adminList = adminMapper.selectByExample(example);
        if (adminList != null && adminList.size() > 0) {
            return adminList.get(0);
        }
        return null;
    }


    @Override
    public UmsAdmin getAdminByBelongId(String belongId){
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andIdentityEqualTo(belongId);
        List<UmsAdmin> adminList = adminMapper.selectByExample(example);
        if(adminList!=null && adminList.size()>0){
            return adminList.get(0);
        }
        return null;
    }



    @Override
    public UmsAdmin register(UmsAdminParam umsAdminParam,String identity) {
        UmsAdmin umsAdmin = new UmsAdmin();
        BeanUtils.copyProperties(umsAdminParam, umsAdmin);
        umsAdmin.setCreateTime(new Date());
        umsAdmin.setStatus(1);
        umsAdmin.setIsFirst(1);
        umsAdmin.setIdentity(identity);
        //查询是否有相同用户名的用户
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(umsAdmin.getUsername());
        List<UmsAdmin> umsAdminList = adminMapper.selectByExample(example);
        if (umsAdminList.size() > 0) {
            return null;
        }
        //将密码进行加密操作
        String encodePassword = passwordEncoder.encode(umsAdmin.getPassword());
        umsAdmin.setPassword(encodePassword);
        adminMapper.insert(umsAdmin);
        // 新增用户 给与初始权限
        UmsRelationAdminRole umsRelationAdminRole = new UmsRelationAdminRole();
        umsRelationAdminRole.setAdminId(umsAdmin.getId());
        umsRelationAdminRole.setRoleId(2L);
        relationAdminRoleMapper.insert(umsRelationAdminRole);
        return umsAdmin;
    }

    @Override
    public String login(String username, String password) {
        String token = null;
        //密码需要客户端加密后传递
        try {
            UserDetails userDetails = loadUserByUsername(username);
            if(!passwordEncoder.matches(password,userDetails.getPassword())){
                throw new BadCredentialsException("密码不正确");
            }
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            token = jwtTokenUtil.generateToken(userDetails);
            updateLoginTimeByUsername(username);
            insertLoginLog(username);
        } catch (AuthenticationException e) {
            LOGGER.warn("登录异常:{}", e.getMessage());
        }
        return token;
    }

    @Override
    public String oauthLogin(String belongId) {
        String token = null;
        try {
            UserDetails userDetails = loadUserByBelongId(belongId);
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            token = jwtTokenUtil.generateToken(userDetails);
            updateLoginTimeByUsername(userDetails.getUsername());
            insertLoginLog(userDetails.getUsername());
        } catch (AuthenticationException e) {
            LOGGER.warn("登录异常:{}", e.getMessage());
        }
        return token;
    }

    @Override
    public void insertLoginLog(String username) {
        UmsAdmin admin = getAdminByUsername(username);
        if(admin==null) {
            return;
        }
        UmsLoginLogParam loginLog = new UmsLoginLogParam();
        loginLog.setAdminId(admin.getId());
        loginLog.setCreateTime(new Date());
        // 获取当前请求
        HttpServletRequest request = HttpContextUtil.getHttpServletRequest();
        String ip = IpUtil.getIpAddr(request);
        loginLog.setIp(ip);
        loginLog.setAddress(IpUtil.getCityInfo(ip));
        // 开始获取 当前用户的 系统+浏览器型号
        loginLog.setSystemUserAgent();
        loginLogMapper.insert(loginLog);
    }


    @Override
    public void updateLoginTimeByUsername(String username) {
        UmsAdmin record = new UmsAdmin();
        record.setLoginTime(new Date());
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(username);
        adminMapper.updateByExampleSelective(record, example);
    }
    @Override
    public String refreshToken(String oldToken) {
        return jwtTokenUtil.refreshHeadToken(oldToken);
    }

    @Override
    public UmsAdmin getItem(Long id) {
        return adminMapper.selectByPrimaryKey(id);
    }


    @Override
    public List<UmsResource> getResourceList(Long adminId) {
        return adminRoleRelationDao.getResourceList(adminId);
    }

    @Override
    public List<UmsAdmin> list(String key, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        UmsAdminExample umsAdminExample = new UmsAdminExample();
        UmsAdminExample.Criteria criteria = umsAdminExample.createCriteria();
        if(!StringUtils.isEmpty(key)){
            //查询用户名
            criteria.andUsernameLike("%"+key+"key");
            umsAdminExample.or().andNickNameLike("%"+key+"%");
        }
        return adminMapper.selectByExample(umsAdminExample);
    }

    @Override
    public int updatePassword(UpdateAdminPasswordParam param) {
        if(StrUtil.isEmpty(param.getUsername())
                ||StrUtil.isEmpty(param.getOldPassword())
                ||StrUtil.isEmpty(param.getNewPassword())){
            return -1;
        }
        UmsAdminExample example = new UmsAdminExample();
        example.createCriteria().andUsernameEqualTo(param.getUsername());
        List<UmsAdmin> adminList = adminMapper.selectByExample(example);
        if(CollUtil.isEmpty(adminList)){
            return -2;
        }
        UmsAdmin umsAdmin = adminList.get(0);
        if(!passwordEncoder.matches(param.getOldPassword(),umsAdmin.getPassword())){
            return -3;
        }
        umsAdmin.setPassword(passwordEncoder.encode(param.getNewPassword()));
        adminMapper.updateByPrimaryKey(umsAdmin);
        return 1;
    }

    @Override
    public UserDetails loadUserByUsername(String username){
        //获取用户信息
        UmsAdmin admin = getAdminByUsername(username);
        if (admin != null) {
            List<UmsResource> resourceList = getResourceList(admin.getId());
            return new AdminUserDetails(admin,resourceList);
        }
        throw new UsernameNotFoundException("用户名或密码错误");
    }

    @Override
    public UserDetails loadUserByBelongId(String belongId){
        //获取用户信息
        UmsAdmin admin = getAdminByBelongId(belongId);
        if(admin!=null){
            List<UmsResource> resourceList = getResourceList(admin.getId());
            return new AdminUserDetails(admin,resourceList);
        }
        throw  new UsernameNotFoundException("用户名密码错误");

    }

    @Override
    public int update(Long id, UmsAdmin admin) {
        admin.setId(id);
        UmsAdmin rawAdmin = adminMapper.selectByPrimaryKey(id);
        if(rawAdmin.getPassword().equals(admin.getPassword())){
            //与原加密密码相同的不需要修改
            admin.setPassword(null);
        }else{
            //与原加密密码不同的需要加密修改
            if(StrUtil.isEmpty(admin.getPassword())){
                admin.setPassword(null);
            }else{
                admin.setPassword(passwordEncoder.encode(admin.getPassword()));
            }
        }
        int count = adminMapper.updateByPrimaryKeySelective(admin);
        return count;
    }

    @Override
    public int delete(Long id) {
        int count = adminMapper.deleteByPrimaryKey(id);
        return count;
    }

    @Override
    public List<UmsRole> getRoleList(Long adminId) {
        return adminRoleRelationDao.getRoleList(adminId);
    }

    @Override
    public int updateRole(Long adminId, List<Long> roleIds) {
        int count = roleIds == null ? 0 : roleIds.size();
        //先删除原来的关系
        UmsRelationAdminRoleExample adminRoleRelationExample = new UmsRelationAdminRoleExample();
        adminRoleRelationExample.createCriteria().andAdminIdEqualTo(adminId);
        roleMapper.deleteByExample(adminRoleRelationExample);
        //建立新关系
        if (!CollectionUtils.isEmpty(roleIds)) {
            List<UmsRelationAdminRole> list = new ArrayList<>();
            for (Long roleId : roleIds) {
                UmsRelationAdminRole roleRelation = new UmsRelationAdminRole();
                roleRelation.setAdminId(adminId);
                roleRelation.setRoleId(roleId);
                list.add(roleRelation);
            }
            adminRoleRelationDao.insertList(list);
        }
        return count;
    }




    @Override
    public String relationAccount(UmsAdminParam umsAdminParam,Integer record) {
        //1、 查询存储的QQ账号信息
        UmsOauthQq oauthQq = oauthQqMapper.selectByPrimaryKey(record);
        //2、 根据传入的类型 判断为绑定已有账号还是新账号; 1:已有账号 2:新账号
        UmsAdmin umsAdmin = new UmsAdmin();
        int count=0;
        // 已存在账号
        if(umsAdminParam.getType().equals("1")){
            //根据用户名 查询站内账号信息
            umsAdmin = getAdminByUsername(umsAdminParam.getUsername());
            UserDetails userDetails = loadUserByUsername(umsAdminParam.getUsername());
            if(!passwordEncoder.matches(umsAdminParam.getPassword(),userDetails.getPassword())){
                throw new BadCredentialsException("密码不正确");
            }
            //3、 关联站内账号与QQ账号信息
            oauthQq.setBelongId(umsAdmin.getIdentity());
        }
        // 未存在账号 需新注册一个账号并绑定
        if(umsAdminParam.getType().equals("2")){
            umsAdminParam.setIcon(oauthQq.getImgUrl());
            umsAdminParam.setNickName(oauthQq.getNickName());
            String identity = UUID.randomUUID().toString();
            umsAdmin = register(umsAdminParam,identity);
        }
        if (umsAdmin == null) {
            return "已存在账号了！！！！";
        }
        count = oauthQqMapper.updateByPrimaryKeySelective(oauthQq);
        String token="";
        // 生成验证 authenticationToken
        if(count>0)token = oauthLogin(umsAdmin.getIdentity());
        return token;
    }

}
