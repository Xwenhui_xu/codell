package com.org.codell.service.ums.impl;

import com.github.pagehelper.PageHelper;
import com.org.codell.service.ums.UmsResourceService;
import com.org.codell.dmg.mapper.*;
import com.org.codell.dmg.model.UmsResource;
import com.org.codell.dmg.model.UmsResourceExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * 后台资源管理Service实现类
 */
@Service
public class UmsResourceServiceImpl implements UmsResourceService {
    @Autowired
    private UmsResourceMapper resourceMapper;

    @Override
    public List<UmsResource> listAll() {
        return resourceMapper.selectByExample(new UmsResourceExample());
    }

    @Override
    public List<UmsResource> list(Long categoryId,String nameKeyword, String urlKeyWord, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        UmsResourceExample umsResourceExample = new UmsResourceExample();
        UmsResourceExample.Criteria criteria = umsResourceExample.createCriteria();
        if(categoryId!=null){
            criteria.andIdEqualTo(categoryId);
        }
        if(!StringUtils.isEmpty(nameKeyword)){
            criteria.andNameLike("%"+nameKeyword+"%");
        }
        if(!StringUtils.isEmpty(urlKeyWord)){
            criteria.andUrlLike("%"+urlKeyWord+"%");
        }
        return resourceMapper.selectByExample(umsResourceExample);
    }


    @Override
    public int create(UmsResource umsResource) {
        umsResource.setCreateTime(new Date());
        return resourceMapper.insert(umsResource);
    }

    @Override
    public int update(Long id, UmsResource umsResource) {
        umsResource.setId(id);
        int count = resourceMapper.updateByPrimaryKeySelective(umsResource);
        return count;
    }

    @Override
    public UmsResource getItem(Long id) {
        return resourceMapper.selectByPrimaryKey(id);
    }

    @Override
    public int delete(Long id) {
        int count = resourceMapper.deleteByPrimaryKey(id);
        return count;
    }
}
