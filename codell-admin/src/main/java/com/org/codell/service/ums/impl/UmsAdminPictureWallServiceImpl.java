package com.org.codell.service.ums.impl;

import com.org.codell.dao.ums.UmsAdminPictureWallDao;
import com.org.codell.dmg.mapper.UmsAdminMapper;
import com.org.codell.dmg.mapper.UmsAdminPictureWallMapper;
import com.org.codell.dmg.model.UmsAdmin;
import com.org.codell.dmg.model.UmsAdminPictureWall;
import com.org.codell.dmg.model.UmsAdminPictureWallExample;
import com.org.codell.dto.UmsAdminPictureWallParam;
import com.org.codell.service.ums.UmsAdminPictureWallService;
import com.org.codell.utils.fileUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UmsAdminPictureWallServiceImpl implements UmsAdminPictureWallService {

    @Value("${prop.upload-folder}")
    private  String UPLOAD_FOLDER;

    @Resource
    private UmsAdminPictureWallDao pictureWallDao;
    @Resource
    private UmsAdminPictureWallMapper wallMapper;
    @Resource
    private UmsAdminMapper adminMapper;

    @Override
    public Integer addPictureWallList(UmsAdminPictureWallParam record) {
        //根据 用户id查询 用户账号
        UmsAdmin umsAdmin = adminMapper.selectByPrimaryKey(record.getAdminId().longValue());
        UmsAdminPictureWall pictureWall = new UmsAdminPictureWall();
        List<String> fileListUrl = new ArrayList<>();
        //处理有图片的时候
        if(record.getFileList()!=null && record.getFileList().size()>0){
            List<MultipartFile> fileList = record.getFileList();
            fileList.forEach(e->{
                fileListUrl.add(fileUtil.approvalFile(UPLOAD_FOLDER,umsAdmin.getUsername(),e));
            });
        }
        BeanUtils.copyProperties(record,pictureWall);
        pictureWall.setCreateTime(new Date());
        pictureWall.setStatus("1");
        if(StringUtils.isEmpty(record.getSort())){
            pictureWall.setSort("0");
        }

        Integer res = pictureWallDao.insertPictureWallForeach(pictureWall,fileListUrl);
        return res;
    }

    @Override
    public List<UmsAdminPictureWall> list(Long adminId) {
        UmsAdminPictureWallExample example = new UmsAdminPictureWallExample();
        example.createCriteria().andAdminIdEqualTo(adminId.intValue());
        List<UmsAdminPictureWall> wallList = wallMapper.selectByExample(example);
        return wallList;
    }
}
