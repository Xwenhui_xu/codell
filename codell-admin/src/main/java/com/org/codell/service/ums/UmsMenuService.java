package com.org.codell.service.ums;

import com.org.codell.dmg.model.UmsMenu;
import com.org.codell.dto.UmsMenuNode;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UmsMenuService {

    /**
     * 分页查询后台菜单
     */
    List<UmsMenu> list(Long parentId,Integer pageNum,Integer pageSize);

    /**
     * 创建后台菜单
     */
    int create(UmsMenu umsMenu);

    /**
     * 修改后台菜单
     */
    int update(Long id, UmsMenu umsMenu);

    /**
     * 根据ID获取菜单详情
     */
    UmsMenu getItem(Long id);

    /**
     * 根据ID删除菜单
     */
    int delete(Long id);
    /**
     * 修改菜单显示状态
     */
    int updateHidden(Long id, Integer hidden);

    List<UmsMenuNode> treeList();
}
