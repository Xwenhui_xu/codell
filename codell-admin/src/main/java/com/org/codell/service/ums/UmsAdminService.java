package com.org.codell.service.ums;

import com.org.codell.dmg.model.UmsAdmin;
import com.org.codell.dmg.model.UmsResource;
import com.org.codell.dmg.model.UmsRole;
import com.org.codell.dto.UmsAdminParam;
import com.org.codell.dto.UpdateAdminPasswordParam;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 后台管理员Service
 */
public interface UmsAdminService {
    /**
     * 根据用户名获取后台管理员
     */
    UmsAdmin getAdminByUsername(String username);


    /**
     * 根据身份 Id 获取后台管理员信息
     */
    UmsAdmin getAdminByBelongId(String belongId);

    /**
     * 注册功能
     */
    UmsAdmin register(UmsAdminParam umsAdminParam,String identity);


    /**
     * 登录功能
     * @param username 用户名
     * @param password 密码
     * @return 生成的JWT的token
     */
    String login(String username, String password);

    String oauthLogin(String belongId);

    /**
     * 根据用户名修改登录时间
     */
    void updateLoginTimeByUsername(String username);

    /**
     * 添加登录记录
     * @param username 用户名
     */
    void insertLoginLog(String username);

    /**
     * 刷新token的功能
     * @param oldToken 旧的token
     */
    String refreshToken(String oldToken);

    /**
     * 根据用户名id获取用户
     */
    UmsAdmin getItem(Long id);

    /**
     * 获取用户信息
     */
    UserDetails loadUserByUsername(String username);

    /**
     * 获取用户信息 根据belongId
     */
    UserDetails loadUserByBelongId(String belongId);

    /**
     * 修改指定用户信息
     */
    int update(Long id, UmsAdmin admin);

    /**
     * 删除指定用户
     */
    int delete(Long id);

    /**
     * 获取用户对于角色
     */
    List<UmsRole> getRoleList(Long adminId);

    /**
     * 获取用户资源列表
     * @param adminId
     * @return
     */
    List<UmsResource> getResourceList(Long adminId);

    /**
     * 获取用户列表
     * @param key
     * @param pageSize
     * @param pageNum
     * @return
     */
    List<UmsAdmin> list(String key,Integer pageSize,Integer pageNum);
    /**
     * 修改密码
     */
    int updatePassword(UpdateAdminPasswordParam updatePasswordParam);


    /**
     * 修改用户角色关系
     */
    @Transactional
    int updateRole(Long adminId, List<Long> roleIds);


    /**
     * 绑定站内账号以后返回token
     */
    @Transactional
    String relationAccount(UmsAdminParam umsAdminParam,Integer record);
}
