package com.org.codell.service.ums.impl;

import com.github.pagehelper.PageHelper;
import com.org.codell.dao.ums.UmsRoleDao;
import com.org.codell.dmg.mapper.UmsRelationRoleMenuMapper;
import com.org.codell.dmg.mapper.UmsRelationRoleResourceMapper;
import com.org.codell.dmg.mapper.UmsRoleMapper;
import com.org.codell.dmg.model.*;
import com.org.codell.service.ums.UmsRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.awt.*;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 后台角色管理Service实现类
 */
@Service
public class UmsRoleServiceImpl implements UmsRoleService {
    @Autowired
    private UmsRoleDao umsRoleDao;
    @Autowired
    private UmsRoleMapper umsRoleMapper;
    @Autowired
    private UmsRelationRoleResourceMapper relationRoleResourceMapper;
    @Autowired
    private UmsRelationRoleMenuMapper relationRoleMenuMapper;

    @Override
    public int create(UmsRole role) {
        role.setCreateTime(new Date());
        role.setAdminCount(0);
        role.setSort(0);
        return umsRoleMapper.insert(role);
    }

    @Override
    public int update(Long id, UmsRole role) {
        role.setId(id);
        return umsRoleMapper.updateByPrimaryKeySelective(role);
    }

    @Override
    public int delete(List<Long> ids) {
        UmsRoleExample example = new UmsRoleExample();
        example.createCriteria().andIdIn(ids);
        int count = umsRoleMapper.deleteByExample(example);
        return count;
    }

    @Override
    public List<UmsMenu> getMenuList(Long adminId) {
        return umsRoleDao.getMenuList(adminId);
    }


    @Override
    public List<UmsRole> list() {
        return umsRoleMapper.selectByExample(new UmsRoleExample());
    }

    @Override
    public List<UmsRole> list(String keywork, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        UmsRoleExample umsRoleExample = new UmsRoleExample();
        UmsRoleExample.Criteria criteria = umsRoleExample.createCriteria();
        if(!StringUtils.isEmpty(keywork)){
            criteria.andNameLike("%"+keywork+"%");
        }
        return umsRoleMapper.selectByExample(umsRoleExample);
    }

    @Override
    public List<UmsMenu> listMenu(Long roleId) {
        return umsRoleDao.getMenuListByRoleId(roleId);
    }


    @Override
    public List<UmsResource> listResource(Long roleId) {
        return umsRoleDao.getResourceListByRoleId(roleId);
    }

    @Override
    public int allocResource(Long roleId, List<Long> resourceIds) {
        //先删除原有关系
        UmsRelationRoleResourceExample example=new UmsRelationRoleResourceExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        List<UmsRelationRoleResource> relationRoleResources=relationRoleResourceMapper.selectByExample(example);
        //获取现有的资源id
        List<Long> relationList = relationRoleResources.stream().map(UmsRelationRoleResource::getResourceId).collect(Collectors.toList());
        //需要删除 交集
        List<Long> dVal = relationList.stream().filter(item -> !resourceIds.contains(item)).collect(Collectors.toList());
        //需要新增 交集
        List<Long> iVal = resourceIds.stream().filter(item -> !relationList.contains(item)).collect(Collectors.toList());
        Integer dStatus=0,istatus=0;
        if(dVal.size()>0){
            //执行批量删除
            dStatus = umsRoleDao.deleteForeachResource(roleId,dVal);
        }
        if(iVal.size()>0){
            //执行批量新增
            istatus = umsRoleDao.insertForeachResource(roleId,iVal);
        }

        int resInt = dStatus>0 && istatus>0 ? 1 : 0;
        return resInt;
    }

    @Override
    public int allocMenu(Long roleId, List<Long> menuIds){
        //查询原有的关系
        UmsRelationRoleMenuExample example = new UmsRelationRoleMenuExample();
        example.createCriteria().andRoleIdEqualTo(roleId);
        List<UmsRelationRoleMenu> relationRoleMenus =relationRoleMenuMapper.selectByExample(example);
        //获取现有的资源id
        List<Long> relationIdList = relationRoleMenus.stream().map(UmsRelationRoleMenu::getMenuId).collect(Collectors.toList());
        //需要删除的资源  交集
        List<Long> dVal = relationIdList.stream().filter(item -> !menuIds.contains(item)).collect(Collectors.toList());
        //需要新增的资源 交集
        List<Long> iVal = menuIds.stream().filter(item -> !relationIdList.contains(item)).collect(Collectors.toList());
        Integer dStatus=0,istatus=0;
        if(dVal.size()>0){
            //执行批量删除
            dStatus = umsRoleDao.deleteForeachMenu(roleId,dVal);
        }
        if(iVal.size()>0){
            //执行批量新增
            istatus = umsRoleDao.insertForeachMenu(roleId,iVal);
        }

        int resInt = dStatus>0 && istatus>0 ? 1 : 0;
        return resInt;
    }



}
