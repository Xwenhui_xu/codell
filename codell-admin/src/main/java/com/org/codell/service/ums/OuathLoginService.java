package com.org.codell.service.ums;

import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

public interface OuathLoginService {
    /**
     * 用户校验
     */
    @Transactional
    Map<String,Object> checkUser(String code, String state) throws Exception;

    /**
     * 返回登录地址
     * @return
     */
    String getCodeByType() throws Exception;
}
