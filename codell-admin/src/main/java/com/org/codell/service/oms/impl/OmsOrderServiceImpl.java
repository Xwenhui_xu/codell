package com.org.codell.service.oms.impl;

import com.github.pagehelper.PageHelper;
import com.org.codell.dao.oms.OmsOrderDao;
import com.org.codell.dmg.mapper.OmsOrderMapper;
import com.org.codell.dmg.model.OmsOrder;
import com.org.codell.dmg.model.OmsOrderExample;
import com.org.codell.dto.OmsOrderDetail;
import com.org.codell.dto.OmsOrderQueryParam;
import com.org.codell.service.oms.OmsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 订单管理Service实现类
 */
@Service
public class OmsOrderServiceImpl implements OmsOrderService {
    @Autowired
    private OmsOrderMapper orderMapper;
    @Autowired
    private OmsOrderDao orderDao;

    @Override
    public List<OmsOrder> list(OmsOrderQueryParam queryParam, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        return orderDao.getList(queryParam);
    }
    @Override
    public int close(List<Long> ids, String note) {
        OmsOrder record = new OmsOrder();
        record.setStatus(4);
        OmsOrderExample example = new OmsOrderExample();
        example.createCriteria().andDeleteStatusEqualTo(0).andIdIn(ids);
        int count = orderMapper.updateByExampleSelective(record, example);
//        List<OmsOrderOperateHistory> historyList = ids.stream().map(orderId -> {
//            OmsOrderOperateHistory history = new OmsOrderOperateHistory();
//            history.setOrderId(orderId);
//            history.setCreateTime(new Date());
//            history.setOperateMan("后台管理员");
//            history.setOrderStatus(4);
//            history.setNote("订单关闭:"+note);
//            return history;
//        }).collect(Collectors.toList());
//        orderOperateHistoryDao.insertList(historyList);
        return count;
    }

    @Override
    public int delete(List<Long> ids) {
        OmsOrder record = new OmsOrder();
        record.setDeleteStatus(1);
        OmsOrderExample example = new OmsOrderExample();
        example.createCriteria().andDeleteStatusEqualTo(0).andIdIn(ids);
        return orderMapper.updateByExampleSelective(record, example);
    }

    @Override
    public OmsOrderDetail detail(Long id) {
        return orderDao.getDetail(id);
    }
}
