package com.org.codell.dao.pms;

import com.org.codell.dto.PmsProductResult;
import org.apache.ibatis.annotations.Param;

public interface PmsProductDao {

    PmsProductResult findProductById(Long id);
}

