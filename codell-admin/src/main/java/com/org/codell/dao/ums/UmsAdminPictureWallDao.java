package com.org.codell.dao.ums;

import com.org.codell.dmg.model.UmsAdminPictureWall;

import java.util.List;

public interface UmsAdminPictureWallDao {

    Integer insertPictureWallForeach(UmsAdminPictureWall pictureWall, List<String> fileListUrl);
}
