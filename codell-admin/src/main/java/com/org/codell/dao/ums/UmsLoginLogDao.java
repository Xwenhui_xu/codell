package com.org.codell.dao.ums;

import com.org.codell.dmg.model.UmsAdmin;
import com.org.codell.vo.UmsLoginLogVo;

import java.util.List;
import java.util.Map;

public interface UmsLoginLogDao {

    List<UmsLoginLogVo> getList(String keyword, String startTime, String endTime);

    Long findTodayVisitCount();

    Long findTodayIp();

    /**
     * 获取系统近10天来的访问记录
     *
     * @return 系统近七天来的访问记录
     */
    List<Map<String, Object>> findLastTenDaysVisitCount(String username);

    /**
     * 获取系统近15天来的访问记录 不足则补零
     *
     * @return 系统近七天来的访问记录
     */
    List<Map<String,Object>> findLastSevenDaysVisitCount(String username);


    /**
     * 按本月统计 周一至周五的所有请求数据
     * @param username
     * @return
     */
    List<Map<String,Object>> findTheMonthByWeeksTotal(String username);
}
