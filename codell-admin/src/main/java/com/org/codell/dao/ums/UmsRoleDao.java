package com.org.codell.dao.ums;

import com.org.codell.dmg.model.UmsMenu;
import com.org.codell.dmg.model.UmsRelationRoleResource;
import com.org.codell.dmg.model.UmsResource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 自定义后台角色管理Dao
 */
public interface UmsRoleDao {
    /**
     * 根据后台用户ID获取菜单
     */
    List<UmsMenu> getMenuList(@Param("adminId") Long adminId);

    /**
     * 根据角色ID获取菜单
     */
    List<UmsMenu> getMenuListByRoleId(@Param("roleId") Long roleId);
    /**
     * 根据角色ID获取资源
     */
    List<UmsResource> getResourceListByRoleId(@Param("roleId") Long roleId);

    /**
     * 批量插入角色+资源关系
     *
     */
    Integer insertForeachResource(Long roleId,List<Long> allList);

    /**
     * 批量删除 角色+资源关系
     */
    Integer deleteForeachResource(Long roleId,List<Long> allList);
    /**
     * 批量插入 角色+菜单关系
     */
    Integer insertForeachMenu(Long roleId,List<Long> allList);

    /**
     * 批量删除  角色+菜单关系
     *
     */
    Integer deleteForeachMenu(Long roleId,@Param("list") List<Long> allList);
}
