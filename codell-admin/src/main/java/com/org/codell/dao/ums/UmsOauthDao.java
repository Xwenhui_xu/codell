package com.org.codell.dao.ums;

import com.org.codell.dmg.model.UmsOauthQq;

/**
 * 第三方认证
 * @author XWH
 */
public interface UmsOauthDao {
    UmsOauthQq findQQAccountByOpenId(String openId);
}
