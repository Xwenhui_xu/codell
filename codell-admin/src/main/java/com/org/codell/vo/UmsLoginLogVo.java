package com.org.codell.vo;

import com.org.codell.dmg.model.UmsAdminLoginLog;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//登录返回的信息
public class UmsLoginLogVo extends UmsAdminLoginLog {
    private String userName;
}
