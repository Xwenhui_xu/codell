package com.org.codell.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 登陆常量配置类
 */
@Configuration
@Data
public class Constants {
    @Value("${oaouth.github.clientId}")
    private String githubClientId;
    @Value("${oaouth.github.clientSecret}")
    private String githubClientSecret;
    @Value("${oaouth.github.redirectUri}")
    private String githubRedirectUri;

    @Value("${oaouth.qq.clientId}")
    private String qqClientId;
    @Value("${oaouth.qq.clientSecret}")
    private String qqClientSecret;
    @Value("${oaouth.qq.redirectUri}")
    private String qqRedirectUri;


}
