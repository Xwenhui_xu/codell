package com.org.codell.component;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.json.JSONUtil;
import com.org.codell.Aspect.WebLog;
import com.org.codell.dmg.mapper.WmsWeblogMapper;
import com.org.codell.dmg.model.WmsWeblog;
import com.org.codell.security.utils.JwtTokenUtil;
import com.org.codell.utils.IpUtil;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.marker.Markers;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 统一日志处理切面
 */
@Aspect
@Component
@Order(1)
public class WebLogAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebLogAspect.class);
    @Autowired
    private WmsWeblogMapper weblogMapper;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Pointcut("execution(public * com.org.codell.controller.*.*.*(..))")
    public void webLog(){
    }
    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable{
    }
    @AfterReturning(value = "webLog()",returning = "ret")
    public void doAfterReturning(Object ret) throws Throwable{

    }
    @Around("webLog()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable{
        Date startTimeDate = new Date();
        long startTime = System.currentTimeMillis();
        //获取当前请求的对象
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        WmsWeblog wmsWeblog = new WmsWeblog();
        //返回结果
        Object result = joinPoint.proceed();
        String authHeader = request.getHeader(this.tokenHeader);
        if (authHeader != null && authHeader.startsWith(this.tokenHead) && (!request.getRequestURI().equals("/admin/info"))){
            String authToken = authHeader.substring(this.tokenHead.length());
            String username = jwtTokenUtil.getUserNameFromToken(authToken);
            wmsWeblog.setUsername(username);
            Signature signature = joinPoint.getSignature();
            MethodSignature methodSignature = (MethodSignature)signature;
            Method method = methodSignature.getMethod();
            if(method.isAnnotationPresent(ApiOperation.class)){
                ApiOperation log = method.getAnnotation(ApiOperation.class);
                wmsWeblog.setDescription(log.value());
            }   
            long endTime = System.currentTimeMillis();
            String urlStr = request.getRequestURL().toString();
            wmsWeblog.setBasepath(StrUtil.removeSuffix(urlStr, URLUtil.url(urlStr).getPath()));
            wmsWeblog.setMethod(request.getMethod());
            // 获取 请求方法中的 参数
            wmsWeblog.setParameter(JSONUtil.toJsonStr(getParameter(method,joinPoint.getArgs())));
            wmsWeblog.setSpendtime((int)(endTime-startTime));
            wmsWeblog.setStarttime(startTimeDate);
            wmsWeblog.setUri(request.getRequestURI());
            wmsWeblog.setUrl(request.getRequestURL().toString());
            String ip = IpUtil.getIpAddr(request);
            wmsWeblog.setIp(ip);
            wmsWeblog.setAddress(IpUtil.getCityInfo(ip));
            weblogMapper.insertSelective(wmsWeblog);
        }

        return result;
    }


    /**
     * 根据方法和传入的参数获取请求参数
     */
    private Object getParameter(Method method, Object[] args) {
        List<Object> argList = new ArrayList<>();
        Parameter[] parameters = method.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            //将RequestBody注解修饰的参数作为请求参数
            RequestBody requestBody = parameters[i].getAnnotation(RequestBody.class);
            if (requestBody != null) {
                argList.add(args[i]);
            }
            //将RequestParam注解修饰的参数作为请求参数
            RequestParam requestParam = parameters[i].getAnnotation(RequestParam.class);
            if (requestParam != null) {
                Map<String, Object> map = new HashMap<>();
                String key = parameters[i].getName();
                if (!StringUtils.isEmpty(requestParam.value())) {
                    key = requestParam.value();
                }
                map.put(key, args[i]);
                argList.add(map);
            }
        }
        if (argList.size() == 0) {
            return null;
        } else if (argList.size() == 1) {
            return argList.get(0);
        } else {
            return argList;
        }
    }

}
