package com.org.codell.component;

import com.org.codell.service.ums.OuathLoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class LoginComponent {
    @Autowired
    private final Map<String, OuathLoginService> loginMap = new ConcurrentHashMap<>();

    public LoginComponent(Map<String,OuathLoginService> loginMap){
        this.loginMap.clear();
        loginMap.forEach(this.loginMap::put);
    }

    public OuathLoginService getLoginService(String componentName) {
        return this.loginMap.get(componentName);
    }
}
