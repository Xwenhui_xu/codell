package com.org.codell.utils;

import com.org.codell.common.tools.CommonResult;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Slf4j
public class fileUtil {

    public static String approvalFile(String UPLOAD_FOLDER,String username,MultipartFile filecontent){
        OutputStream os = null;
        InputStream inputStream = null;
        String resUrl="";
        //获取文件后缀
        String suffix="";
        try {
            inputStream = filecontent.getInputStream();
            suffix = filecontent.getOriginalFilename().substring(filecontent.getOriginalFilename().lastIndexOf(".") + 1, filecontent.getOriginalFilename().length());
            if (!"jpg,jpeg,gif,png".toUpperCase().contains(suffix.toUpperCase())) {
                return "请选择jpg,jpeg,gif,png格式的图片";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            // 1K的数据缓冲
            byte[] bs = new byte[1024];
            // 读取到的数据长度
            int len;
            Date date = new Date();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            //生成用户名+日期
            String fileUrl= username+File.separator+df.format(date);

            File tempFile = new File(UPLOAD_FOLDER+ File.separator + fileUrl);
            if (!tempFile.exists()) {
                tempFile.mkdirs();
            }
            //通过UUID生成唯一文件名
            String filename = UUID.randomUUID().toString().replaceAll("-","") + "." + suffix;
            // 输出的文件流保存到本地文件
            os = new FileOutputStream(tempFile.getPath() + File.separator  + filename);
            // 开始读取
            while ((len = inputStream.read(bs)) != -1) {
                os.write(bs, 0, len);
            }
            resUrl=fileUrl+ File.separator +filename;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 完毕，关闭所有链接
            try {
                os.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }return resUrl;
    }
}
