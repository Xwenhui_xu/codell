package com.org.codell.controller.sms;

import com.org.codell.common.tools.CommonPage;
import com.org.codell.common.tools.CommonResult;
import com.org.codell.service.sms.SmsMonitorService;
import com.org.codell.service.ums.UmsAdminService;
import com.org.codell.vo.UmsLoginLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@Api(tags = "SmsMonitorController",description = "系统监控中心管理")
@RequestMapping(value = "/monitor")
public class SmsMonitorController {

    @Resource
    private SmsMonitorService monitorService;



}
