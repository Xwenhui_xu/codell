package com.org.codell.controller.ums;

import com.alibaba.fastjson.JSON;
import com.org.codell.common.LoginConsTantEnum;
import com.org.codell.common.tools.CommonResult;
import com.org.codell.component.LoginComponent;
import com.org.codell.dto.UmsAdminParam;
import com.org.codell.service.ums.UmsAdminService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping(value = "/oauth")
public class UmsOauthController {
    @Autowired
    private LoginComponent loginComponent;
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private UmsAdminService adminService;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    /**
     * 返回第三方登录请求地址
     *
     * @return
     */
    @GetMapping("/{type}/getCode")
    @ResponseBody
    public CommonResult getCode(@PathVariable("type") String type) throws Exception {
        log.info("获取{}的登录地址............", type);
        LoginConsTantEnum.TypeEnum typeEnum = LoginConsTantEnum.TypeEnum.getInstance(type);
        if (null == typeEnum) {
            log.info("............登录类型不对............");
            return CommonResult.validateFailed("暂无" + type + "功能");
        }
        String url = loginComponent.getLoginService(typeEnum.getClassName()).getCodeByType();
        return CommonResult.success(url);
    }

    /**
     * 验证用户名密码
     *
     * @param type
     * @return
     */
    @GetMapping(value = "/{type}/callback")
    @ResponseBody
    public String login(@PathVariable("type") String type,
                        @RequestParam(value = "code", required = false) String code,
                        @RequestParam(value = "state", required = false) String state,
                        HttpServletResponse response) {
        // 响应编码
        response.setContentType("text/html; charset=utf-8");
        Map<String, Object> msg = new HashMap<>();
        try {
            log.info("使用{}登录方式验证............", type);
            LoginConsTantEnum.TypeEnum typeEnum = LoginConsTantEnum.TypeEnum.getInstance(type);
            if (null == typeEnum) {
                log.info("............登录类型不对............");
            }
            msg = loginComponent.getLoginService(typeEnum.getClassName()).checkUser(code, state);
            msg.put("tokenHead", tokenHead);
        } catch (Exception e) {
            log.error("LoginController login error type={}", type, e);
        }
        return "<script>window.onload = function () {\n" +
                "        window.opener.postMessage('" + JSON.toJSONString(msg) + "', 'http://www.codell.cloud/#/login');\n" +
//                "        window.opener.postMessage('" + JSON.toJSONString(msg) + "', 'http://localhost:9528/#/login');\n" +
                "        window.close();" +
                "    }</script>";
    }

// Github 测试
// https://github.com/login/oauth/authorize?client_id=f69e7db149d59d125d30&redirect_uri=http://127.0.0.1:8095/oauth/github/callback&state=codell&scope=user

//qq 本地测试
// https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=101905073&redirect_uri=http%3A%2F%2F127.0.0.1%3A8095%2Foauth%2Fqq%2Fcallback&state=ok

//qq 远程测试
// https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=101905073&redirect_uri=http%3A%2F%2Fwww.codell.cloud%3A8095%2Foauth%2Fqq%2Fcallback&state=ok


    @ApiOperation(value = "绑定站内账号以返回token")
    @RequestMapping(value = "/relationAccount/{record}", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult relationAccount(@RequestBody UmsAdminParam umsAdminParam, @PathVariable Integer record) {
        String token = adminService.relationAccount(umsAdminParam, record);
        if (token == null || token.equals("createError")) {
            return CommonResult.validateFailed(token);
        }
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return CommonResult.success(tokenMap);
    }


    @GetMapping(value = "/{type}/smsCode")
    @ResponseBody
    @ApiOperation(value = "发送短信验证码")
    public CommonResult smsCode(@PathVariable String type, @RequestParam(value = "mobile", required = false) String mobile) {
        String code = "1234";
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("1357499868@qq.com");//发送者
        msg.setTo("\n" +
                "2622158369@qq.com");//接收者
        msg.setSubject("邮箱标题");//标题
        msg.setText("https://www.cnblogs.com/lin02/");//内容
        javaMailSender.send(msg);
        return CommonResult.success(code);
    }


}