package com.org.codell.controller.ums;

import com.org.codell.common.tools.CommonResult;
import com.org.codell.dto.UmsAdminPictureWallParam;
import com.org.codell.service.ums.UmsAdminPictureWallService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.PrivateKey;

/**
 * 管理端 照片墙
 */
@Controller
@Api(tags = "UmsAdminPictureWallController",description = "管理员照片墙管理")
@RequestMapping(value = "/picture")
public class UmsAdminPictureWallController {


    @Resource
    private UmsAdminPictureWallService  pictureWallService;

    @ApiOperation("新增个人管理墙可上传多张")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public CommonResult add(UmsAdminPictureWallParam record){
        return CommonResult.success(pictureWallService.addPictureWallList(record));
    }

    @ApiOperation("查询个人照片墙")
    @GetMapping(value = "/list/{adminId}")
    @ResponseBody
    public CommonResult list(@PathVariable Long adminId){
        return CommonResult.success(pictureWallService.list(adminId));
    }
}
