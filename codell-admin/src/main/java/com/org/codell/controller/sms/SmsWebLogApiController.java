package com.org.codell.controller.sms;

import com.org.codell.common.tools.CommonPage;
import com.org.codell.common.tools.CommonResult;
import com.org.codell.dmg.model.WmsWeblog;
import com.org.codell.service.sms.SmsWebLogApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@Api(tags = "SmsWebLogApiController",description = "API请求追踪管理")
@RequestMapping(value = "/webLogApi")
public class SmsWebLogApiController {
    @Resource
    private SmsWebLogApiService webLogApiService;


    @ApiOperation("根据用户名或昵称查询请求追踪日志列表")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CommonPage<WmsWeblog>> list(@RequestParam(value = "keyword",required = false)String keyword,
                                                    @RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
                                                    @RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){
        List<WmsWeblog> weblogList = webLogApiService.list(keyword,pageNum,pageSize);
        return CommonResult.success(CommonPage.restPage(weblogList));
    }

    @ApiOperation("批量/单条删除请求追踪日志")
    @RequestMapping(value = "/delete")
    @ResponseBody
    public CommonResult delete(@RequestParam("ids") List<Integer> ids) {
        int count = webLogApiService.delete(ids);
        if (count > 0) {
            return CommonResult.success(count);
        }
        return CommonResult.failed();
    }

}
