package com.org.codell.controller.ums;

import com.org.codell.common.tools.CommonPage;
import com.org.codell.common.tools.CommonResult;
import com.org.codell.service.ums.UmsLoginLogService;
import com.org.codell.vo.UmsLoginLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@Api(tags = "UmsUserLoginLog", description = "用户登录日志管理")
@RequestMapping(value = "/loginLog")
public class UmsUserLoginLogController {

    @Autowired
    private UmsLoginLogService loginLogService;

    @ApiOperation(value = "首页查询日志记录")
    @RequestMapping(value = "/index/{username}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult index(@PathVariable String username) {
        Map<String, Object> result = new HashMap<>();
        //获取总访问数
        result.put("totalVisitCount", loginLogService.findTotalVisitCount());
        //获取进入总访问数
        result.put("todayVisitCount", loginLogService.findTodayVisitCount());
        //获取系统今日访问 IP数
        result.put("todayIpCount", loginLogService.findTodayIp());
        return CommonResult.success(result);
    }

    @ApiOperation(value = "首页查询近15天登录日志")
    @RequestMapping(value = "/fifteen/{username}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult Fifteen(@PathVariable String username) {
        Map<String, Object> result = new HashMap<>();
        //获取近七天访问数
        result.put("lastUserSevenVisitCount", loginLogService.findLastTenDaysVisitCount(username));
        //获取近七天所有访问
        result.put("lastAllSevenVisitCount", loginLogService.findLastTenDaysVisitCount(null));
        return CommonResult.success(result);
    }

    @ApiOperation(value = "首页查询网关请求按本月统计每周")
    @RequestMapping(value = "/theMonth/{username}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult theMonth(@PathVariable String username) {
        Map<String, Object> result = new HashMap<>();
        //按本月统计 周一至周五的所有请求数据
        result.put("userTheMonthByWeeksTotal", loginLogService.findTheMonthByWeeksTotal(username));
        //按本月统计 周一至周五的所有请求数据
        result.put("allTheMonthByWeeksTotal", loginLogService.findTheMonthByWeeksTotal(null));
        return CommonResult.success(result);
    }


    @ApiOperation("根据用户名或昵称查询登录日志列表")
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CommonPage<UmsLoginLogVo>> loginLogList(@RequestParam(value = "keyword", required = false) String keyword,
                                                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                                                @RequestParam(value = "startTime", required = false) String startTime,
                                                                @RequestParam(value = "endTime", required = false) String endTime) {
        List<UmsLoginLogVo> umsLoginLogVoList = loginLogService.list(keyword, startTime, endTime, pageNum, pageSize);
        return CommonResult.success(CommonPage.restPage(umsLoginLogVoList));
    }

    @ApiOperation("批量/单条删除登陆日志")
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    @ResponseBody
    public CommonResult delete(@RequestParam("ids") List<Long> ids) {
        int count = loginLogService.delete(ids);
        if (count > 0) {
            return CommonResult.success(count);
        }
        return CommonResult.failed();
    }
}
