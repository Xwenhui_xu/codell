package com.org.codell.dto;

import lombok.Data;

@Data
public class GithubUser {
    private String name;
    private Long id;
    private String avatar_url;
    private String bio;
    private String node_id;
    private String login;
    private String html_url;
}
