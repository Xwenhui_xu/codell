package com.org.codell.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
public class UmsAdminPictureWallParam {
    @ApiModelProperty(value = "用户id")
    private Integer adminId;
    @ApiModelProperty(value = "排序")
    private String sort;
    @ApiModelProperty(value = "图片文件")
    private List<MultipartFile> fileList;

}
