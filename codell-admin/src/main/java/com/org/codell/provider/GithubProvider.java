package com.org.codell.provider;

import com.alibaba.fastjson.JSON;
import com.org.codell.dto.AccessTokenDTO;
import com.org.codell.dto.GithubUser;
import com.org.codell.utils.HttpClientUtils;
import org.springframework.stereotype.Component;
import static com.org.codell.utils.HttpClientUtils.post;

@Component
public class GithubProvider {
    /**
     * 使用httpclient根据传过来的accessTokenDTO对象，向https://github.com/login/oauth/access_token发送post请求获得token
     *
     * @param accessTokenDTO
     * @return
     */
    public String getAccessToken(AccessTokenDTO accessTokenDTO) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append("https://github.com/login/oauth/access_token");
        String str = post("https://github.com/login/oauth/access_token", JSON.toJSONString(accessTokenDTO),
                "application/json", "UTF-8", 10000, 10000);
        return str.split("&")[0].split("=")[1];
    }

    /**
     * 使用httpclient根据传过来的accessToken向https://api.github.com/user发送请求获得user
     *
     * @param accessToken
     * @return
     */
    public GithubUser  getUser(String accessToken) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append("https://api.github.com/user?");
        url.append("access_token=" + accessToken);
        // 获取用户相关数据
        String result = HttpClientUtils.get(url.toString(), "UTF-8");
        return JSON.parseObject(result, GithubUser.class);

    }
}
